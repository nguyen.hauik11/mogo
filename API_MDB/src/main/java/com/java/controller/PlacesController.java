package com.java.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.java.entity.Files;
import com.java.entity.Places;
import com.java.entity.Regions;
import com.java.entity.Place.AllFile;
import com.java.entity.Place.Banner;
import com.java.entity.Place.FilesUri;
import com.java.entity.Place.Loc;
import com.java.entity.Place.Logo;
import com.java.repository.RegionRepository;
import com.java.service.FileService;
import com.java.service.PlacesService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api")
public class PlacesController {

	@Autowired
	PlacesService placesService;

	@Autowired
	RegionRepository regionsService;

	@Autowired
	FileService fileService;

	@RequestMapping(value = "/findAll/{page}/{size}", method = RequestMethod.GET)
	public ResponseEntity<List<Places>> findAll(@PathVariable("page") int page, @PathVariable("size") int size) {

		PageRequest pageable = PageRequest.of(page, size);
		Page<Places> listPlaces = placesService.findByPage(pageable);

		if (listPlaces.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<List<Places>>(listPlaces.getContent(), HttpStatus.OK);

	}

	public Files saveFiles(MultipartFile file) {
		Files files2 = new Files();
		try {
			File newFile = new File(
					"C:\\Users\\NGUYEN\\eclipse-workspace\\mogo\\API_MDB\\src\\image\\" + file.getOriginalFilename());
			FileOutputStream fileOS = null;
			fileOS = new FileOutputStream(newFile);
			fileOS.write(((MultipartFile) file).getBytes());
			files2.set_id(ObjectId.get());
			files2.setUri(newFile.getPath());
			fileOS.close();
		} catch (Exception e) {
			log.error("error", e);
		}

		return files2;

	}

	private Loc upLoc(Double a, Double b) {
		Loc loc = new Loc();
		loc.setType("Point");
		ArrayList<Double> array = new ArrayList<>();
		
		if (-90 <= a && a <= 90 && -180 <= b && b <= 180) {
			array.add(0, a);
			array.add(1, b);
			loc.setCoordinates(array);
		} else {
			return null;
		}

		return loc;

	}

	@PostMapping("/savePlace")
	public Places savePlaces(@Valid @RequestParam("name") String name, @Valid @RequestParam("address") String address,
			@Valid @RequestParam("short_description") String short_description,
			@Valid @RequestParam("regionId") ObjectId regionId, @Valid @RequestParam("description") String description,
			@Valid @RequestParam("loc[lat]") Double a,
			@Valid @RequestParam("files[banner]") List<MultipartFile> filesBanner,
			@Valid @RequestParam("loc[long]") Double b,
			@Valid @RequestParam("files[logo]") List<MultipartFile> fileLogo,
			@Valid @RequestParam("weight") double weight) {
		Optional<Regions> regions = regionsService.findById(regionId);
		
		if (regionId != null) {
			Places places = new Places();
			AllFile allFile = new AllFile();
			ArrayList<Banner> listBanner = new ArrayList<>();
			ArrayList<Logo> listLogo = new ArrayList<>();
			Banner banner = new Banner();
			Logo logo = new Logo();
			HashMap<ObjectId, String> listFileUriBanner = new HashMap<>();
			HashMap<ObjectId, String> listFileUriLogo = new HashMap<>();

			for (MultipartFile files : filesBanner) {
				Files files2 = saveFiles(files);
				fileService.saveFile(files2);
				banner.set_id(ObjectId.get());
				listBanner.add(banner);
				listFileUriBanner.put(files2.get_id(), files2.getUri());
			}

			allFile.setBanner(listBanner);

			for (MultipartFile file : fileLogo) {
				Files files2 = saveFiles(file);
				fileService.saveFile(files2);
				logo.set_id(ObjectId.get());
				listLogo.add(logo);
				listFileUriLogo.put(files2.get_id(), files2.getUri());
			}

			allFile.setLogo(listLogo);
			places.setFiles(allFile);
			Loc loc = upLoc(a, b);
			places.setLoc(loc);
			FilesUri filesUri = new FilesUri();
			filesUri.setBanner(listFileUriBanner);
			filesUri.setLogo(listFileUriLogo);
			places.setFilesUri(filesUri);
			places.set_id(ObjectId.get());
			places.setName(name);
			places.setAddress(address);
			places.setShort_description(short_description);
			places.setDescription(description);
			places.setRegion(regions.get().get_id());
			places.setUser(regionId);
			places.setCreated(LocalDate.now());
			places.setModified(LocalDate.now());
			places.setWeight(weight);
			
			return placesService.save(places);
		}
		
		return null;
	}

	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET)
	public Optional<Places> getById(@PathVariable("id") ObjectId id) {
		Optional<Places> places = placesService.getOne(id);
		
		if (places == null) {
			return null;
		}
		return places;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deletePlaces(@PathVariable("id") ObjectId id) {
		placesService.delete(id);
	}

	@RequestMapping(value = "/put/{id}", method = RequestMethod.PUT)
	public Object update(@PathVariable("id") ObjectId id, @RequestParam("name") String name,
			@RequestParam("address") String address, @RequestParam("short_description") String short_description,
			@RequestParam("description") String description, @RequestParam("loc[lat]") Double x,
			@RequestParam("loc[long]") Double y, @RequestParam("region") ObjectId region) {
		Optional<Places> places2 = placesService.getOne(id);
		Optional<Regions> regions = regionsService.findById(region);
		Loc loc = new Loc();

		if (places2 == null || regions == null) {
			return ResponseEntity.notFound().build();
		}
		
		Places places = new Places();
		places.set_id(id);
		places.setName(name);
		places.setAddress(address);
		places.setShort_description(short_description);
		places.setDescription(description);
		loc = upLoc(x, y);
		places.setLoc(loc);
		places.setRegion(region);
		places.setUser(region);
		places.setFiles(places2.get().getFiles());
		places.getFiles();
		places.setFilesUri(places2.get().getFilesUri());
		places.setCreated(places2.get().getCreated());
		places.setModified(LocalDate.now());
		
		return ResponseEntity.ok(placesService.save(places));
	}

}
