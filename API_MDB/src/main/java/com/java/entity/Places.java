package com.java.entity;

import java.time.LocalDate;
import java.util.ArrayList;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.java.entity.Place.AllFile;
import com.java.entity.Place.FilesUri;
import com.java.entity.Place.Loc;
import com.java.validator.Address;
import com.java.validator.Name;

import lombok.Data;

@Document(value = "places")
@Data
public class Places {

	@Id
	private ObjectId _id;

	private Double weight;

	private String country_code;

	private ObjectId region;

	private ArrayList<ObjectId> categories;

	private String collections;

	@NotBlank
	@Size(max = 255)
	@Name
	private String name;

	private String name_ascii;

	@NotBlank
	@Address
	@Size(max = 500)
	private String address;

	@NotBlank
	@Size(max = 1000)
	private String short_description;

	@NotBlank
	@Size(max = 5000)
	private String description;

	private ArrayList<String> tags;

	private ArrayList<String> tags_ascii;

	private String tel;

	private String website;

	private String email;

	private AllFile files;

	private String lang_code;

	private int status;

	private ObjectId user;

	private LocalDate created;

	private LocalDate modified;

	private Double view_count;

	@NotNull
	private Loc loc;

	private String url_alias;

	private FilesUri filesUri;

	public String get_id() {
		return _id.toHexString();
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getRegion() {
		return region.toHexString();
	}

	public void setRegion(ObjectId region) {
		this.region = region;
	}

	public String getUser() {
		return user.toHexString();
	}

	public void setUser(ObjectId user) {
		this.user = user;
	}

	public void setFiles(AllFile files) {
		this.files = files;
	}

}
