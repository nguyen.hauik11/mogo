package com.java.entity.Place;

import java.util.ArrayList;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class AllFile {

	@NotBlank
	private ArrayList<Banner> banner;
	private ArrayList<Logo> logo;
}
