package com.java.entity.Place;

import java.util.HashMap;

import org.bson.types.ObjectId;

import lombok.Data;

@Data
public class FilesUri {

	private HashMap<ObjectId, String> banner;
	private HashMap<ObjectId, String> logo;
	
}
