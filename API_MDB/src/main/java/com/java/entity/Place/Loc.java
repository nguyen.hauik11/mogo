package com.java.entity.Place;

import java.util.ArrayList;

import lombok.Data;

@Data
public class Loc {

	private String type;

	private ArrayList<Double> coordinates;
}
