package com.java.entity.Place;

import org.bson.types.ObjectId;

public class Logo {

	private ObjectId _id;

	public String get_id() {
		return _id.toHexString();
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}
	
}
