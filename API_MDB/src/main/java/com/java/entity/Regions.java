package com.java.entity;

import java.time.LocalDate;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import com.java.entity.Place.Loc;

import lombok.Data;

@Data
@Document(collection = "regions")
public class Regions {

	private ObjectId _id;

	private String name;

	private String code_name;

	private String country_code;

	private Double status;

	private Loc loc;

	private ObjectId location;
	
	private String lang_code;
	
	private ObjectId user;
	
	private LocalDate modified;
	
	private LocalDate created;
}
