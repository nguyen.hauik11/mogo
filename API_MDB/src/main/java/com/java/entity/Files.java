package com.java.entity;

import javax.validation.constraints.NotBlank;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection="file")
public class Files {

	@Id
	private ObjectId _id;
	@NotBlank
	private String uri;

}
