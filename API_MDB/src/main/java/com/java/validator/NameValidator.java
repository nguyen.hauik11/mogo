package com.java.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NameValidator implements ConstraintValidator<Name, String> {

	@Override
	public boolean isValid(String name, ConstraintValidatorContext context) {
		if (name == null) {
			return false;
		}
		
		String a = "^\\D{1,}$";
		boolean matcher = Pattern.matches(a, name);
		
		return matcher;
	}

}
