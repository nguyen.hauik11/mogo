package com.java.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AddressValidator implements ConstraintValidator<Address, String> {

	@Override
	public boolean isValid(String Address, ConstraintValidatorContext context) {
		if (Address == null) {
			return false;
		}
		String a="^\\D{1,}$";
		boolean matcher=Pattern.matches(a, Address);
		return matcher;
	}

}
