package com.java.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DescriptionValidator implements ConstraintValidator<Description, String> {

	
	@Override
	public boolean isValid(String description, ConstraintValidatorContext context) {
		if (description==null) {
			return false;
		}
		String a="^\\w{1,}$";
		boolean matcher = Pattern.matches(a, description);
		return matcher;
	}

}
