package com.java.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class Short_descriptionValidator  implements ConstraintValidator<Short_description, String>{

	@Override
	public boolean isValid(String Short_description, ConstraintValidatorContext context) {
		if(Short_description==null) {
			return false;
		}
		String a="^\\w{1,}$";
		boolean matcher=Pattern.matches(a, Short_description);
		return matcher;
	}

}
