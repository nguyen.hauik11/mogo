package com.java.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Target({ ElementType.FIELD, ElementType.METHOD })
@Constraint(validatedBy = DescriptionValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface Description {
	String message() default "{Error Validator}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
