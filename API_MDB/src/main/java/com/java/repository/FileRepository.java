package com.java.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.java.entity.Files;

@Repository
public interface FileRepository extends MongoRepository<Files, ObjectId> {

}
