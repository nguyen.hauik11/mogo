package com.java.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.entity.Regions;

public interface RegionRepository extends MongoRepository<Regions, ObjectId> {

}
