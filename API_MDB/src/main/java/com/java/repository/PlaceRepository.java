package com.java.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.java.entity.Places;

@Repository
public interface PlaceRepository extends MongoRepository<Places, ObjectId> {
    
}
