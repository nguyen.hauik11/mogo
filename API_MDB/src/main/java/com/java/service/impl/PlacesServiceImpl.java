package com.java.service.impl;

import java.util.List;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.java.entity.Places;
import com.java.repository.PlaceRepository;
import com.java.service.PlacesService;

@Configuration
public class PlacesServiceImpl implements PlacesService {
	@Autowired
	PlaceRepository placesRepository;

	@Override
	public List<Places> findAll() {
		return placesRepository.findAll();
	}

	@Override
	public Places save(Places places) {
		return placesRepository.save(places);
	}

	@Override
	public Optional<Places> getOne(ObjectId id) {
		Optional<Places> places = placesRepository.findById(id);
		return places;
	}

	@Override
	public void delete(ObjectId _id) {
		placesRepository.deleteById(_id);

	}

	@Override
	public Page<Places> findByPage(PageRequest pageRequest) {
		return placesRepository.findAll(pageRequest);
	}

}
