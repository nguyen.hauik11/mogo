package com.java.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.java.entity.Files;
import com.java.repository.FileRepository;
import com.java.service.FileService;

@Configuration
public class FileServiceImpl implements FileService {

	@Autowired
	FileRepository fileRepository;

	@Override
	public Files saveFile(Files file) {
		return fileRepository.save(file);
	}

}
