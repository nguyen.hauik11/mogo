package com.java.service.impl;

import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.java.entity.Regions;
import com.java.repository.RegionRepository;
import com.java.service.RegionsService;

@Configuration
public class RegionsServiceImpl implements RegionsService {

	@Autowired
	RegionRepository regionsRepository;

	@Override
	public Boolean exsit(ObjectId _id) {
		return regionsRepository.existsById(_id);
	}

	@Override
	public Optional<Regions> findBtId(ObjectId id) {
		return regionsRepository.findById(id);
	}

}
