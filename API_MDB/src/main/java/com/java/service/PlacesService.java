package com.java.service;

import java.util.List;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.java.entity.Places;

public interface PlacesService {

	public List<Places> findAll();

	public Places save(Places places);

	public Optional<Places> getOne(ObjectId _id);

	public void delete(ObjectId _id);

	public Page<Places> findByPage(PageRequest pageRequest);
}
