package com.java.service;

import java.util.Optional;

import org.bson.types.ObjectId;

import com.java.entity.Regions;

public interface RegionsService {

	public Boolean exsit(ObjectId _id);

	Optional<Regions> findBtId(ObjectId _id);
}
